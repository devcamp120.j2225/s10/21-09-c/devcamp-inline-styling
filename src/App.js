import image from "./assets/images/user.jpg";

function App() {
  return (
    <div>
      <div style={{width: "800px", margin: "0 auto", textAlign: "center", border: "1px solid #ddd", marginTop: "100px", paddingBottom: "50px"}}>
        <div style={{marginTop: "-50px"}}>
          <img src={image} alt="avatar user" style={{width: "100px", borderRadius: "50%"}}></img>
        </div>
        <div>
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <div style={{fontSize: "12px"}}>
          <b>Tammy Stevens</b> &nbsp; * &nbsp; Front End Developer
        </div>
      </div>
    </div>
  );
}

export default App;
